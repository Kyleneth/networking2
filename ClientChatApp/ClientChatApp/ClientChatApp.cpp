// ClientChatApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include "PongServer.h"
#include "PongClient.h"
#include "PongGraphics.h"
#include <stdio.h>

RakNet::AddressOrGUID ServerAddress;

int main(int argc, char* args[])
{
	
	SDL_Init(SDL_INIT_VIDEO);
	srand(time(NULL));
	//The window we'll be rendering to
	PongGraphics* pgFunk = new PongGraphics();

	if (!pgFunk->init())
	{
		printf("Failed to initialize!\n");
		pgFunk->quit_the_game();
	}
	else
	{
		if (!pgFunk->loadMedia())
		{
			printf("Failed to load media!\n");
			pgFunk->quit_the_game();
		}
		if (!pgFunk->setup_score_font()) {
			fprintf(stderr, "Unable to load font!\n");
			std::cin.get();
			pgFunk->quit_the_game();
		}
	}
	pgFunk->set_initial_coordinates_for_images();
	pgFunk->run_game_loop();

	pgFunk->quit_the_game();
	
	RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
	bool isServer = false;
	PongServer* server;
	PongClient* client;

	std::cout << "(c) for client and (s) for server: \n";
	char input;
	std::cin >> input;

	if (input == 's' || input == 'S')
	{
		server = new PongServer();
		server->RunServer(peer);
		isServer = true;
	}
	else
	{
		client = new PongClient();
		client->RunClient(peer);

	}

	printf("My GUID is %s\n", peer->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());

	char aMessageLine[2046];
	while (true)
	{
		//If I am the client, then run an input loop
		if (_kbhit() && !isServer)
		{
			std::cin.getline(aMessageLine, 2046);
			client->SendStringToPeer(aMessageLine, (RakNet::MessageID)ID_SERVER_MESSAGE_READ, ServerAddress, peer);
		}
		if (isServer)
		{
			server->RunCommon(peer);
		}
		else
		{
			client->RunCommon(peer);
		}
	}

	std::cin.get();
    return 0;
}

