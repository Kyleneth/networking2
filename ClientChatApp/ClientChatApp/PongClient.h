#pragma once
#include "stdafx.h"

class PongClient
{
public:
	PongClient();
	~PongClient();
	unsigned char GetPacketIdentifier(RakNet::Packet *p);
	RakNet::RakString GetStringFromPacket(RakNet::Packet* aPacket);
	void RunClient(RakNet::RakPeerInterface* client);
	void RunCommon(RakNet::RakPeerInterface* peer);
	void SendStringToPeer(std::string aMessage, RakNet::MessageID messageID, 
		RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer);
};

