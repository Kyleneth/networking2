#include "stdafx.h"
#include "PongClient.h"


PongClient::PongClient()
{
}


PongClient::~PongClient()
{
}

unsigned char PongClient::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

RakNet::RakString PongClient::GetStringFromPacket(RakNet::Packet* aPacket)
{
	RakNet::RakString aName;
	RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(aName);
	return aName;

}

void PongClient::RunClient(RakNet::RakPeerInterface* client)
{
	//Don't allow someone-else to respond and accept my connection request
	client->AllowConnectionResponseIPMigration(false);

	RakNet::SocketDescriptor socketDescriptor;
	socketDescriptor.socketFamily = AF_INET;
	//Need only one connection at a time, since we only connect to server
	client->Startup(1, &socketDescriptor, 1);
	client->SetOccasionalPing(true);

	RakNet::ConnectionAttemptResult car = client->Connect
	("localhost", 200, "Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));

	//Make sure we get an "error" if we cannot connect.
	//Great for debugging
	RakAssert(car == RakNet::CONNECTION_ATTEMPT_STARTED);

	printf("\nMy IP addresses AS A CLIENT:\n");
	unsigned int i;
	for (i = 0; i < client->GetNumberOfAddresses(); i++)
	{
		printf("%i. %s\n", i + 1, client->GetLocalIP(i));
	}
}

void PongClient::SendStringToPeer(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer)
{
	// Use a BitStream to write a custom user message
	// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
	RakNet::BitStream bsOut;
	bsOut.Write(messageID);
	bsOut.Write(aMessage.c_str());

	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, false);

}

void PongClient::RunCommon(RakNet::RakPeerInterface* peer)
{
	RakNet::Packet* aPacket = nullptr;

	for (aPacket = peer->Receive(); aPacket; peer->DeallocatePacket(aPacket), aPacket = peer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		auto packetIdentifier = GetPacketIdentifier(aPacket);

		// Check if this is a network message packet
		switch (packetIdentifier)
		{
			// CLIENT
		case ID_ALREADY_CONNECTED:
			// Connection lost normally
			printf("ID_ALREADY_CONNECTED with guid %" PRINTF_64_BIT_MODIFIER "u\n", aPacket->guid);
			break;
		case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_CONNECTION_LOST\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
			printf("ID_REMOTE_NEW_INCOMING_CONNECTION\n");
			break;
		case ID_CONNECTION_BANNED: // Banned from this server
			printf("We are banned from this server.\n");
			break;
		case ID_CONNECTION_ATTEMPT_FAILED:
			printf("Connection attempt failed\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			// Sorry, the server is full.  I don't do anything here but
			// A real app should tell the user
			printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
			break;

		case ID_INVALID_PASSWORD:
			printf("ID_INVALID_PASSWORD\n");
			break;

		case ID_CONNECTION_LOST:
			// Couldn't deliver a reliable packet - i.e. the other system was abnormally
			// terminated
			printf("ID_CONNECTION_LOST\n");
			break;
			// COMMON
		case ID_DISCONNECTION_NOTIFICATION:
			// Connection lost normally
			printf("ID_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
			break;

		case ID_READY_MESSAGE_1:
		{
			std::cout << "READY...\n";
		}
		break;

		case ID_CLIENT_MESSAGE_READ:
		{
			RakNet::RakString aMessage = GetStringFromPacket(aPacket);
			std::cout << aMessage.C_String() << std::endl;
		}
		break;

		default:
			RakNet::RakString aString = GetStringFromPacket(aPacket);
			std::cout << aString << std::endl;

			//printf("\nUNIDENTIFIED PACKET!");
			break;
		}
	}
}