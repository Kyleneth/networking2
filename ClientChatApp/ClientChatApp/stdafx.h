// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <conio.h>

#include "RakPeerInterface.h"
#include "RakNetTypes.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include <cstdio>
#include <iostream>
#include <cassert>

#include<string>
#include <vector>
#include <map>

enum GameMessages
{
	ID_WELCOME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
	ID_GETNAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 2,
	ID_READY_MESSAGE_1 = ID_USER_PACKET_ENUM + 3,
	ID_SERVER_MESSAGE_READ = ID_USER_PACKET_ENUM + 4,
	ID_CLIENT_MESSAGE_READ = ID_USER_PACKET_ENUM + 5

};


// TODO: reference additional headers your program requires here
