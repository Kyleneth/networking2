#include "stdafx.h"
#include "PongGraphics.h"


PongGraphics::PongGraphics()
{
	mWindow = NULL;
	mScreenSurface = NULL;
	mBackground = NULL;
}

bool PongGraphics::init()
{
	bool success = true;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		mWindow = SDL_CreateWindow("Pong", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

		if (mWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			mScreenSurface = SDL_GetWindowSurface(mWindow);
		}
	}
	return success;
}


bool PongGraphics::loadMedia()
{
	bool success = true;
	mBackground = SDL_LoadBMP("../Media/Background.bmp");
	if (mBackground == NULL)
	{
		printf("Unable to load image %s! Error: %s\n", "../Media/Background.bmp", SDL_GetError());
		success = false;
	}
	SDL_Surface *paddle = SDL_LoadBMP("../Media/paddle.bmp");
	if (paddle == NULL) {
		printf("Unable to load image %s! Error: %s\n", "../Media/paddle.bmp", SDL_GetError());
		success = false;
	}

	m1Paddle.surface = paddle;
	m2Paddle.surface = paddle;

	SDL_Surface *ball = SDL_LoadBMP("../Media/lawson_dean.bmp");
	if (ball == NULL) {
		printf("Unable to load image %s! Error: %s\n", "../Media/lawson_dean.bmp", SDL_GetError());
		success = false;
	}
	mBall.surface = ball;

	return success;
}

void PongGraphics::update()
{
	SDL_BlitSurface(mBackground, NULL, mScreenSurface, NULL);
}
bool PongGraphics::setup_score_font() {

	if (TTF_Init() != 0) {
		return false;
	}

	score_font = TTF_OpenFont("../Media/SAVEDBYZ.TTF", 32);
	if (score_font == NULL) {
		return false;
	}

	return true;
}
void PongGraphics::set_initial_coordinates_for_images() {

	m1Paddle.x = 0;
	m1Paddle.y = rand_between(0, SCREEN_HEIGHT - m1Paddle.surface->h);

	m2Paddle.x = SCREEN_WIDTH - m2Paddle.surface->w;
	m2Paddle.y = rand_between(0, SCREEN_HEIGHT - m2Paddle.surface->h);

	pong_ball_reset();
}

void PongGraphics::run_game_loop(void) {

	Uint32 start_ticks = 0, end_ticks = 0;
	SDL_Event event;
	const Uint8 *keystate = NULL;

	for (;;) {


		while (SDL_PollEvent(&event)) {

			switch (event.type) {

			case SDL_QUIT:
				quit_the_game();
			}
		}

		keystate = SDL_GetKeyboardState(NULL);

		handle_keystate(keystate);
		redraw_all_images();

		pong_ball_handle_collision();

	}
}

void PongGraphics::handle_keystate(const Uint8 *keystate) {

	if (keystate[LPADDLE_UP_KEY]) {
		paddle_move(&m1Paddle, DIRECTION_UP);
	}

	if (keystate[LPADDLE_DOWN_KEY]) {
		paddle_move(&m1Paddle, DIRECTION_DOWN);
	}

	if (keystate[RPADDLE_UP_KEY]) {
		paddle_move(&m2Paddle, DIRECTION_UP);
	}

	if (keystate[RPADDLE_DOWN_KEY]) {
		paddle_move(&m2Paddle, DIRECTION_DOWN);
	}

	if (keystate[START_GAME_KEY]) {

		if (!mBall.moving) {

			pong_ball_set_in_motion();
		}
	}

	if (mBall.moving) {
		pong_ball_move();
	}

	if (keystate[QUIT_GAME_KEY]) {
		quit_the_game();
	}
}

void PongGraphics::paddle_move(paddleData *paddle, unsigned int direction) {

	int difference =
		image_get_acceptable_y_movement(paddle->surface, paddle->y,
			PADDLE_SPEED, direction);

	paddle->y += difference;
}

int PongGraphics::image_get_acceptable_y_movement(SDL_Surface *surface, int y, int speed, unsigned int direction) 
{
	int difference = 0;
	if (direction & DIRECTION_UP) {

		if (y - speed < 0) {
			difference = -y;
		}
		else {
			difference = -speed;
		}

	}
	else if (direction & DIRECTION_DOWN) {

		if (y + speed + surface->h > mScreenSurface->h) {
			difference = (mScreenSurface->h - y) - surface->h;
		}
		else {
			difference = +speed;
		}
	}

	return difference;
}

void PongGraphics::pong_ball_set_in_motion() {

	mBall.moving = true;
	mBall.direction = 0;

	mBall.direction |= (rand() % 2 == 0) ? DIRECTION_UP : DIRECTION_DOWN;
	mBall.direction |= (rand() % 2 == 0) ? DIRECTION_LEFT : DIRECTION_RIGHT;

}

void PongGraphics::pong_ball_move(void) {

	int difference =
		image_get_acceptable_y_movement(mBall.surface, mBall.y,
			BALL_SPEED, mBall.direction);

	mBall.y += difference;

	if (abs(difference) != BALL_SPEED) {
		if (mBall.direction & DIRECTION_UP) {
			mBall.direction &= ~DIRECTION_UP;
			mBall.direction |= DIRECTION_DOWN;
		}
		else if (mBall.direction & DIRECTION_DOWN) {
			mBall.direction &= ~DIRECTION_DOWN;
			mBall.direction |= DIRECTION_UP;
		}
	}
	if (mBall.direction & DIRECTION_LEFT) {
		mBall.x -= BALL_SPEED;
	}
	else if (mBall.direction & DIRECTION_RIGHT) {
		mBall.x += BALL_SPEED;
	}

	mBall.hit_paddle = false;
	mBall.out_of_bounds = false;
	if (mBall.direction & DIRECTION_LEFT) {

		if (mBall.x < (m1Paddle.x + m1Paddle.surface->w)) {

			if (mBall.y + mBall.surface->h < m1Paddle.y ||
				mBall.y > m1Paddle.y + m1Paddle.surface->h) {

				mBall.out_of_bounds = true;
			}
			else {

				mBall.hit_paddle = true;
				mBall.x = m1Paddle.x + m1Paddle.surface->w;
			}
		}
	}
	else if (mBall.direction & DIRECTION_RIGHT) {

		if (mBall.x + mBall.surface->w > m2Paddle.x) {
			if (mBall.y + mBall.surface->h < m2Paddle.y ||
				mBall.y > m2Paddle.y + m2Paddle.surface->h) {

				mBall.out_of_bounds = true;
			}
			else {
				mBall.hit_paddle = true;
				mBall.x = m2Paddle.x - mBall.surface->w;
			}
		}
	}
}

void PongGraphics::redraw_all_images() {

	update();

	blit_surface(m1Paddle.surface, mScreenSurface, m1Paddle.x, m1Paddle.y);
	blit_surface(m2Paddle.surface, mScreenSurface, m2Paddle.x, m2Paddle.y);
	blit_surface(mBall.surface, mScreenSurface, mBall.x, mBall.y);
	draw_scores(m1Paddle.score, m2Paddle.score);

	SDL_UpdateWindowSurface(mWindow);
}

int PongGraphics::blit_surface(SDL_Surface *src, SDL_Surface *dest, Sint16 x, Sint16 y) {

	SDL_Rect offset;
	offset.x = x;
	offset.y = y;

	return SDL_BlitSurface(src, NULL, dest, &offset);
}

void PongGraphics::draw_scores(int left_score, int right_score) {

	char lscore_text[10], rscore_text[10];
	snprintf(lscore_text, sizeof(lscore_text), "%d", left_score);
	snprintf(rscore_text, sizeof(rscore_text), "%d", right_score);

	SDL_Surface *lscore_surface = render_text(lscore_text);
	SDL_Surface *rscore_surface = render_text(rscore_text);

	blit_surface(lscore_surface, mScreenSurface,
		(SCREEN_WIDTH / 2) / 2 - lscore_surface->w / 2, 0);

	blit_surface(rscore_surface, mScreenSurface,
		((SCREEN_WIDTH / 2) / 2) + (SCREEN_WIDTH / 2) -
		(rscore_surface->w / 2), 0);

	SDL_FreeSurface(lscore_surface);
	SDL_FreeSurface(rscore_surface);
}

SDL_Surface* PongGraphics::render_text(const char *text) {

	SDL_Color color;
	color.r = 176; color.g = 219; color.b = 255;
	return TTF_RenderText_Solid(score_font, text, color);
}

void PongGraphics::pong_ball_handle_collision() {

	if (mBall.hit_paddle) {

		pong_ball_move_in_opposite_direction();
		mBall.hit_paddle = false;

	}
	else if (mBall.out_of_bounds) {

		if (mBall.direction & DIRECTION_LEFT) {
			m2Paddle.score += 1;
		}
		else if (mBall.direction & DIRECTION_RIGHT) {
			m1Paddle.score += 1;
		}

		pong_ball_reset();
		mBall.out_of_bounds = false;
	}
}
void PongGraphics::pong_ball_move_in_opposite_direction() {

	unsigned int direction = (rand() % 2 == 0) ? DIRECTION_UP : DIRECTION_DOWN;

	if (mBall.direction & DIRECTION_LEFT) {
		direction |= DIRECTION_RIGHT;
	}
	else if (mBall.direction & DIRECTION_RIGHT) {
		direction |= DIRECTION_LEFT;
	}
	printf("%u \n", mBall.direction);

	mBall.direction = direction;
}

void PongGraphics::pong_ball_reset() {

	mBall.x = (SCREEN_WIDTH / 2) - (mBall.surface->w / 2);
	mBall.y = rand_between(0, SCREEN_HEIGHT - mBall.surface->h);
	mBall.moving = false;
}

void PongGraphics::quit_the_game() {

	SDL_FreeSurface(m1Paddle.surface);
	SDL_FreeSurface(mBall.surface);

	if (TTF_WasInit()) {
		TTF_CloseFont(score_font);
		TTF_Quit();
	}
	/*
	delete mWindow;
	mWindow = NULL;
	delete mScreenSurface;
	mScreenSurface = NULL;
	delete mBackground;
	mBackground = NULL;
	*/
	//Deallocate surface
	SDL_FreeSurface(mBackground);
	mBackground = NULL;

	//Destroy window
	SDL_DestroyWindow(mWindow);
	mWindow = NULL;

	SDL_Quit();

	exit(0);
}