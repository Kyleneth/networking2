#pragma once

#include <SDL.h>
#include <SDL_ttf.h>
#include <time.h>
#include <stdlib.h>

//const int SCREEN_WIDTH = 640;
//const int SCREEN_HEIGHT = 480;
#define SCREEN_WIDTH 640		
#define SCREEN_HEIGHT 480		
#define BITS_PER_PIXEL 32	
#define LPADDLE_UP_KEY SDL_SCANCODE_W	  	//player1 moves
#define LPADDLE_DOWN_KEY SDL_SCANCODE_S
#define RPADDLE_UP_KEY SDL_SCANCODE_UP	   	//player2 moves
#define RPADDLE_DOWN_KEY SDL_SCANCODE_DOWN
#define QUIT_GAME_KEY SDL_SCANCODE_ESCAPE  	
#define START_GAME_KEY SDL_SCANCODE_SPACE  
#define PADDLE_SPEED 2		   	
#define BALL_SPEED 1		   	
#define rand_between(min, max) (rand() % ((max)-(min) + 1)) + (min)

enum {
	DIRECTION_UP = (1 << 0),
	DIRECTION_DOWN = (1 << 1),
	DIRECTION_LEFT = (1 << 2),
	DIRECTION_RIGHT = (1 << 3)
};

/* Structure definition to hold paddle-specific data. */
struct paddleData {

	SDL_Surface *surface;	//image of paddle
	int x, y;			//position
	int score;			//player score
};

struct ballData {

	SDL_Surface *surface;	//imge of ball
	int x, y;			//position of ball
	bool moving;		//is it moving
	unsigned int direction;	
	bool out_of_bounds;		//is it out of bounds
	bool hit_paddle;		//did it collide
};

class PongGraphics
{
public:
	PongGraphics();
	~PongGraphics();

	bool loadMedia();
	bool init();
	void update();
	bool setup_score_font();
	void run_game_loop();
	void quit_the_game();
	void handle_keystate(const Uint8 *keystate);
	void paddle_move(paddleData *paddle, unsigned int direction);
	void redraw_all_images();
	void set_initial_coordinates_for_images();
	void pong_ball_reset();
	void pong_ball_set_in_motion();
	void pong_ball_move();
	void pong_ball_handle_collision();
	void pong_ball_move_in_opposite_direction();
	void draw_scores(int left_score, int right_score);
	int blit_surface(SDL_Surface *src, SDL_Surface *dest, Sint16 x, Sint16 y);
	int image_get_acceptable_y_movement(SDL_Surface *surface, int y, int speed, unsigned int direction);
	SDL_Surface* render_text(const char *text);
private:
	SDL_Window* mWindow;
	SDL_Surface* mScreenSurface;
	SDL_Surface* mBackground;
	ballData mBall;
	paddleData m1Paddle;
	paddleData m2Paddle;
	SDL_Event mEvent;
	TTF_Font* score_font;
};	

