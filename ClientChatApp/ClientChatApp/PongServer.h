#pragma once

#include "stdafx.h"

class PongServer
{
public:
	PongServer();
	~PongServer();

	unsigned char GetPacketIdentifier(RakNet::Packet* p);
	void SendStringToPeer(std::string aMessage, RakNet::MessageID messageID,
		RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer);
	void BroadcastStringFromSystemToAll(std::string aMessage, RakNet::MessageID messageID, 
		RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer);
	void BroadcastStringToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::RakPeerInterface* peer);
	RakNet::RakString GetStringFromPacket(RakNet::Packet* aPacket);
	void RunServer(RakNet::RakPeerInterface* server);
	void RunCommon(RakNet::RakPeerInterface* peer);
private:
	std::vector<std::string> ClientList;
	std::map<RakNet::AddressOrGUID, std::string> ClientMap;

};

