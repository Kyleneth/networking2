#include "stdafx.h"
#include "PongServer.h"


PongServer::PongServer()
{
}


PongServer::~PongServer()
{
}

unsigned char PongServer::GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

void PongServer::SendStringToPeer(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer)
{
	// Use a BitStream to write a custom user message
	// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
	RakNet::BitStream bsOut;
	bsOut.Write(messageID);
	bsOut.Write(aMessage.c_str());

	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, false);

}

void PongServer::BroadcastStringFromSystemToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::AddressOrGUID systemAddress, RakNet::RakPeerInterface* peer)
{
	RakNet::BitStream bsOut;
	bsOut.Write(messageID);
	bsOut.Write(aMessage.c_str());
	//UNASSIGNED_SYSTEM_ADDRESS means we dont ignore anyone on the list (no filters)
	//The "true" as the last parameter means we want to broadcast to all
	peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, systemAddress, true);

}

void PongServer::BroadcastStringToAll(std::string aMessage, RakNet::MessageID messageID, RakNet::RakPeerInterface* peer)
{

	BroadcastStringFromSystemToAll(aMessage, messageID, RakNet::UNASSIGNED_SYSTEM_ADDRESS, peer);

}

RakNet::RakString PongServer::GetStringFromPacket(RakNet::Packet* aPacket)
{
	RakNet::RakString aName;
	RakNet::BitStream bsIn(aPacket->data, aPacket->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(aName);
	return aName;

}

void PongServer::RunServer(RakNet::RakPeerInterface* server)
{
	server->SetIncomingPassword("Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	//Concider us disconnected if we don't respond for 3 seconds
	server->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);

	RakNet::SocketDescriptor socketDescriptor;
	socketDescriptor.port = 200;
	socketDescriptor.socketFamily = AF_INET; // Test out IPV4
	bool b = server->Startup(8, &socketDescriptor, 1) == RakNet::RAKNET_STARTED;
	server->SetMaximumIncomingConnections(4);

	//Make sure connection doesn't die out if we havent sent a message in a while
	server->SetOccasionalPing(true);
	//Set "time-out" to 1 sec, but this is only for unreliable messages
	server->SetUnreliableTimeout(1000);

	printf("\nMy IP addresses AS A SERVER:\n");
	for (unsigned int i = 0; i < server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
	}
}

void PongServer::RunCommon(RakNet::RakPeerInterface* peer)
{
	RakNet::Packet* aPacket = nullptr;

	for (aPacket = peer->Receive(); aPacket; peer->DeallocatePacket(aPacket), aPacket = peer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		auto packetIdentifier = GetPacketIdentifier(aPacket);

		// Check if this is a network message packet
		switch (packetIdentifier)
		{
			// SERVER
		case ID_NEW_INCOMING_CONNECTION:
			// Somebody connected.  We have their IP now
			printf("ID_NEW_INCOMING_CONNECTION from %s with GUID %s\n", aPacket->systemAddress.ToString(true), aPacket->guid.ToString());
			ClientList.push_back(aPacket->systemAddress.ToString(true));

			SendStringToPeer("Server: Hello!", (RakNet::MessageID)ID_WELCOME_MESSAGE_1, aPacket->systemAddress, peer);

			break;

		case ID_CONNECTION_LOST:
			// Couldn't deliver a reliable packet - i.e. the other system was abnormally
			// terminated
			printf("ID_CONNECTION_LOST\n");
			break;
			// COMMON
		case ID_DISCONNECTION_NOTIFICATION:
			// Connection lost normally
			printf("ID_DISCONNECTION_NOTIFICATION\n");
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
			break;

		case ID_GETNAME_MESSAGE_1:
		{

			RakNet::RakString aName = GetStringFromPacket(aPacket);
			//ClientMap[aPacket->guid] = aName;
			std::cout << "Connected to: " << aName << std::endl;

			//Tell client we are Ready to receive chat messages now
			SendStringToPeer("", (RakNet::MessageID)ID_READY_MESSAGE_1, aPacket->guid, peer);
		}
		break;

		case ID_READY_MESSAGE_1:
		{
			std::cout << "READY...\n";
		}
		break;

		case ID_SERVER_MESSAGE_READ:
		{
			RakNet::RakString aMessage = GetStringFromPacket(aPacket);
			//BroadcastStringToAll(aMessage.C_String(), (RakNet::MessageID)ID_CLIENT_MESSAGE_READ, peer);
			BroadcastStringFromSystemToAll(aMessage.C_String(), (RakNet::MessageID)ID_CLIENT_MESSAGE_READ, aPacket->guid, peer);
		}
		break;

		default:
			RakNet::RakString aString = GetStringFromPacket(aPacket);
			std::cout << aString << std::endl;

			//printf("\nUNIDENTIFIED PACKET!");
			break;
		}
	}
}
